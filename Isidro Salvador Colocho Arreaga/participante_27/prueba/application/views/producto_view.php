	<center>	
		<br>
		<h1>Tabla de producto</h1>
		<br>
		<div class="container">
			<button data-toggle="modal" data-target="#agregar"  class="btn btn-primary">Agregar producto</button>
			<button data-toggle="modal" data-target="#descrip"  class="btn btn-info">Agregar Categoria</button>
			<table class="table">
				<thead>
					<tr>
						<td>n</td>
						<td>Nombre</td>
						<td>Precio</td>
						<td>Cantidad</td>
						<td>Descripcion</td>
						<td>sumar</td>
						<td>restar</td>
						<td>Eliminar</td>
					</tr>
				</thead>
				<tbody>
					<?php $n=1 ; foreach ($mostrar as $m) { ?>
						<tr>
							<td><?= $n;$n++ ?></td>
							<td><?= $m->nombre ?></td>
							<td><?= $m->precio ?></td>
							<td><?= $m->stock ?></td>
							<td><?= $m->descripcion ?></td>
							<td><a class="btn btn-success btn-sm" href="<?=base_url('producto_controller/operacion/1/'.$m->id_producto)?>">Sumar</a></td>
							<td><a class="btn btn-warning btn-sm" href="<?=base_url('producto_controller/operacion/2/'.$m->id_producto)?>">Restar</a></td>
							<td><a class="btn btn-danger btn-sm" onclick=" return confirm('¿Estas seguro que quieres eliminar este producto?. Perdera todo la informacion del producto.')" href="<?=base_url('producto_controller/eliminar/'.$m->id_producto)?>">Eliminar</a></td>
						</tr>
					<?php  } ?>
				</tbody>	
			</table>
		</div>
	</center>

<div id="agregar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Agregar Producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('producto_controller/producto')?>" method="POST" onsubmit="return producto()==true">
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Nombre</label>
		    <input type="text"  class="form-control" id="nombre_p" name="nombre_p">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Precio</label>
		    <input type="text" class="form-control" id="precio" name="precio">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Stock</label>
		    <input type="number" class="form-control" id="stock" name="stock">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Categoria</label>
		    <select type="text" class="form-control" id="categoria" name="categoria">
		    	<option value="">--Seleccione una Categoria--</option>
		    	<?php foreach ($categoria as $c) {?>
		    		<option value="<?=$c->id_categoria ?>"><?=$c->descripcion ?></option>
		    	<?php  } ?>
		    </select> 
		  </div>
		  <div>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
			<input type="submit" id="categoriaB" class="btn btn-primary" name="Guardar" value="Guardar">
		</div>
		</form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<div id="descrip" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Agregar Categoria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<form action="<?=base_url('producto_controller/categoria')?>" method="POST" onsubmit="return categoria()==true">
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Nombre</label>
		    <input type="text"  class="form-control" id="nombre_c" name="nombre_c">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Descripcion</label>
		    <input type="text" class="form-control" id="descripcion" name="descripcion">
		  </div>
		  <div>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
			<input type="submit" id="descripcionB" class="btn btn-primary" name="Guardar" value="Guardar">
		</div>
		</form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>