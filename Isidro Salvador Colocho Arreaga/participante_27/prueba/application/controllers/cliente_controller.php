<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cliente_controller extends CI_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model("cliente_model");
	}

	public function index(){
		$data = array(
			'title' => 'Tienda || Cliente',
			'mostrar' => $this->cliente_model->mostrar()
		);
		$this->load->view('template/header',$data);
		$this->load->view('cliente_view');
		$this->load->view('template/footer');
	}

	public function detalle($id){
		$data = array(
			'title' => 'Tienda || Factura',
			'factura' => $this->cliente_model->factura($id),
			'detalle' => $this->cliente_model->detalle($id)
		);
		$this->load->view('template/header',$data);
		$this->load->view('detalle_view.php');
		$this->load->view('template/footer');
	}

	public function cliente(){
		$datos['nombre']=$this->input->post('nombre');
		$datos['apellido']=$this->input->post('apellido');
		$datos['direccion']=$this->input->post('direccion');
		$datos['fecha']=$this->input->post('fecha');
		$datos['email']=$this->input->post('email');
		$datos['cel']=$this->input->post('cel');
		$res = $this->cliente_model->cliente($datos);
		if ($res=='add') {
			echo '<script>alert("agregado exitosamente")</script>';
		}else{
			echo '<script>alert("Error al agregar")</script>';
		}
		redirect('cliente_controller','refresh');
	}

	/*public function detalle($id){
		$data = array(
			'title' => 'Tienda || Factura',
			'factura' => $this->cliente_model->factura($id),
			'detalle' => $this->cliente_model->detalle($id)
		);
		$this->load->view('template/header',$data);
		$this->load->view('detalle_view.php');
		$this->load->view('template/footer');
	}*/
}
