function cliente() {
	var nombre = document.getElementById('nombre').value;
	var apellido = document.getElementById('apellido').value;
	var direccion = document.getElementById('direccion').value;
	var cel = document.getElementById('cel').value;
	var fecha = document.getElementById('fecha').value;
	var categoria = document.getElementById('categoria').value;

	if (nombre.length ==0) {
		document.getElementById('nombre').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('nombre').style.boxShadow='0 0 15px green';
	}
	if (nombre.length >50) {
		document.getElementById('nombre').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('nombre').style.boxShadow='0 0 15px green';
	}

	if (apellido.length ==0) {
		document.getElementById('apellido').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('apellido').style.boxShadow='0 0 15px green';
	}
	if (apellido.length >50) {
		document.getElementById('apellido').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('apellido').style.boxShadow='0 0 15px green';
	}

	if (direccion.length ==0) {
		document.getElementById('direccion').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('direccion').style.boxShadow='0 0 15px green';
	}
	if (direccion.length >100) {
		document.getElementById('direccion').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('direccion').style.boxShadow='0 0 15px green';
	}
	
	if (cel ==0) {
		document.getElementById('cel').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('cel').style.boxShadow='0 0 15px green';
	}
	if (cel < 11111111) {
		document.getElementById('cel').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('cel').style.boxShadow='0 0 15px green';
	}
	if (cel > 99999999) {
		document.getElementById('cel').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('cel').style.boxShadow='0 0 15px green';
	}
	return true;
}