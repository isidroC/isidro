<?php
defined('BASEPATH') or exit('No es permitido accesar');
//Class: Implementa el modelo de modoPago
class ModoPagoImp extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->library('ModoPago');
	}

	/*Gestion*/
	/*Fn: Agrega una modoPago
	@param: Objeto modoPago
	@return:na
	*/
	public function create($modoPago){

	}

	/*Fn: Devuelve un listado de modoPagos
	@param:na
	@return: Lista de modoPagos
	*/
	public function read(){
		
	}

	/*Fn: Obtiene una modoPago
	@param: id modoPago
	@return: Instancia de modoPago
	*/
	public function readById($id){
		
	}

	/*Fn: Actualiza los datos de modoPago
	@param: Objeto modoPago
	@return:na
	*/
	public function update($modoPago){

	}

	/*Fn: Quita modoPago de la bd
	@param: id a quitar
	@return:na
	*/
	public function delete($id){

	}
}