<?php
defined('BASEPATH') or exit('No es permitido accesar');
//Class: Implementa el modelo de detalle
class DetalleImp extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->library('Detalle');
	}

	/*Gestion*/
	/*Fn: Agrega una detalle
	@param: Objeto detalle
	@return:na
	*/
	public function create($detalle){

	}

	/*Fn: Devuelve un listado de detalles
	@param:na
	@return: Lista de detalles
	*/
	public function read(){
		
	}

	/*Fn: Obtiene una detalle
	@param: id detalle
	@return: Instancia de detalle
	*/
	public function readById($id){
		
	}

	/*Fn: Actualiza los datos de detalle
	@param: Objeto detalle
	@return:na
	*/
	public function update($detalle){

	}

	/*Fn: Quita detalle de la bd
	@param: id a quitar
	@return:na
	*/
	public function delete($id){

	}
}