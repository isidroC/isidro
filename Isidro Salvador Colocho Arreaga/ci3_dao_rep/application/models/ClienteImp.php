<?php
defined('BASEPATH') or exit('No es permitido accesar');
//Class: Implementa el modelo de cliente
class ClienteImp extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->library('Cliente');
	}

	/*Gestion*/
	/*Fn: Agrega una cliente
	@param: Objeto cliente
	@return:na
	*/
	public function create($cliente){

	}

	/*Fn: Devuelve un listado de clientes
	@param:na
	@return: Lista de clientes
	*/
	public function read(){
		
	}

	/*Fn: Obtiene una cliente
	@param: id cliente
	@return: Instancia de cliente
	*/
	public function readById($id){
		
	}

	/*Fn: Actualiza los datos de cliente
	@param: Objeto cliente
	@return:na
	*/
	public function update($cliente){

	}

	/*Fn: Quita cliente de la bd
	@param: id a quitar
	@return:na
	*/
	public function delete($id){

	}
}