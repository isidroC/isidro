<?php

class Cliente{

	private $nombre;
	private $apellido;
	private $direccion;
	private $fechaNac;
	private $telefono;
	private $correo;
	//Array de instancias
	private $facturas;

	/*Rel*/
	public function getFacturas(){
		return $this->facturas;
	}

	public function setFacturas($facturas){
		$this->facturas = $facturas;
	}

	/*G&S*/
	public function getNombre(){
		return $this->nombre;
	}

	public function setNombre($nombre){
		$this->nombre = $nombre;
	}

	public function getApellido(){
		return $this->apellido;
	}

	public function setApellido($apellido){
		$this->apellido = $apellido;
	}

	public function getDireccion(){
		return $this->direccion;
	}

	public function setDireccion($direccion){
		$this->direccion = $direccion;
	}

	public function getFechaNac(){
		return $this->fechaNac;
	}

	public function setFechaNac($fechaNac){
		$this->fechaNac = $fechaNac;
	}

	public function getTelefono(){
		return $this->direccion;
	}

	public function setTelefono($telefono){
		$this->telefono = $telefono;
	}

	public function getCorreo(){
		return $this->correo;
	}

	public function setCorreo($correo){
		$this->correo = $correo;
	}
}