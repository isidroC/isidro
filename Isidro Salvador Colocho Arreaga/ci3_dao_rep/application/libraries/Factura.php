<?php

class Factura{

	private $fecha;
	//Instancias foraneas para definir la factura
	private $cliente;
	private $modoPago;

	/*Rel*/
	public function getCliente(){
		return $this->cliente;
	}

	public function setCliente($cliente){
		$this->cliente = $cliente;
	}

	public function getModoPago(){
		return $this->modoPago;
	}

	public function setModoPago($modoPago){
		$this->modoPago = $modoPago;
	}

	/*G&S*/
	public function getFecha(){
		return $this->fecha;
	}

	public function setFecha($fecha){
		$this->fecha = $fecha;
	}
}