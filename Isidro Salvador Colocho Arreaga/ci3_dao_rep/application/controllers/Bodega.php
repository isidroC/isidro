<?php
defined('BASEPATH') or exit('No es permitido accesar');

class Bodega extends CI_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('CategoriaImp');
		$this->load->library('pdf');
	}

	/*Fn: Mostrar vista principal de la Bodega
	@param: na
	@return: vista principal*/
	public function index(){
		$this->load->view('templates/header');
		$this->load->view('bodega');
		$this->load->view('templates/footer');
	}

	/*Fn: Crear una categoria
	@param: na
	@return: vista principal*/
	public function frmCategoria(){
		$this->load->view('templates/header');
		$this->load->view('gesBodega/categoria');
		$this->load->view('templates/footer');
	}

	/*Fn: Obtener el listado de categorias
	@param: identificador de categoria
	@return: un objeto categoria*/
	public function byCategory($id=1){
		$ca= $this->CategoriaImp->readById($id);
		echo '<strong>'.$ca->getNombre().':</strong> '.$ca->getDescripcion();
		echo "<br><br>";
	}

	/*Fn: Exportar reporte de categorias
	@param: identificador de categoria
	@return: Reporte de categorias*/
	public function repCategorias(){
		$this->load->view('reporte');
		$html = $this->output->get_output();
		$this->dompdf->loadHtml($html);
		$this->dompdf->setPaper('Letter', 'portrait');
		$this->dompdf->render();
		$this->dompdf->stream("Categoria.pdf", array("Attachment"=>0));
	}
}