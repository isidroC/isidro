<?php
defined('BASEPATH') or exit('No se encuentra acceso a este fragmento');

class Ventas extends CI_Controller{
	public function __construct(){
		parent::__construct();
	}

	/*Fn: Mostrar vista principal de Ventas
	@param: na
	@return: vista principal*/
	public function index(){
		$this->load->view('templates/header');
		$this->load->view('ventas');
		$this->load->view('templates/footer');
	}
}