<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cliente_model extends CI_Model {

	public function mostrar(){
		$this->db->select('f.id_cliente,c.nombre,c.apellido,c.direccion,c.telefono,f.fecha,f.num_factura');
		$this->db->from('factura f');
		$this->db->join('cliente c','c.id_cliente=f.id_cliente');
		$this->db->join('modo_pago mp','mp.num_pago=f.num_pago');
		$exe = $this->db->get();
		return $exe->result();
	}

	public function factura($id){
		$this->db->where('f.num_factura',$id);
		$this->db->select('f.id_cliente,c.nombre,c.apellido,c.direccion,c.telefono,f.fecha,f.num_factura');
		$this->db->from('factura f');
		$this->db->join('cliente c','c.id_cliente=f.id_cliente');
		$this->db->join('modo_pago mp','mp.num_pago=f.num_pago');
		$exe = $this->db->get();
		return $exe->result();
	}

	public function detalle($id){
		$this->db->where('d.id_factura',$id);
		$this->db->select('d.num_detalle,d.id_factura,p.nombre,d.cantidad,d.precio');
		$this->db->from('datalle d');
		$this->db->join('producto p ',' p.id_producto=d.id_producto');
		$exe = $this->db->get();
		return $exe->result();
	}

	public function cliente($datos){
		$this->db->set('nombre',$datos['nombre']);
		$this->db->set('apellido',$datos['apellido']);
		$this->db->set('direccion',$datos['direccion']);
		$this->db->set('fecha_nacimiento',$datos['fecha']);
		$this->db->set('telefono',$datos['cel']);
		$this->db->set('email',$datos['email']);
		$this->db->insert('cliente');
		if ($this->db->affected_rows() > 0) {
			return 'add';
		}else{
			return false;
		}
	}
	
}
