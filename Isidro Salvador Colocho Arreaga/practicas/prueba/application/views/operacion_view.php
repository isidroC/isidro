<center>
	<?php if ($res == 1) {?>
		<br>
		<h1>Sumar producto</h1>	
		<br>
		<div class="container">
		<table class="table">
				<thead>
					<tr>
						<td>n</td>
						<td>Nombre</td>
						<td>Precio</td>
						<td>Cantidad actual</td>
						<td>Cantidad a Operar</td>
						<td>Accion</td>
					</tr>
				</thead>
				<tbody>
					<form action="<?=base_url('producto_controller/agregar')?>" method="POST" onsubmit="return operacion()==true">
						<?php $n=1 ; foreach ($produc as $m) { ?>
							<input type="hidden" name="id" value="<?= $m->id_producto ?>">
							<input type="hidden" name="res" value="<?= $res ?>">
							<tr>
								<td><?= $n;$n++ ?></td>
								<td><?= $m->nombre ?></td>
								<td><?= $m->precio ?></td>
								<td><?= $m->stock ?></td>
								<td><input type="number" name="cant" id="cant"></td>
								<td><input type="submit" class="btn btn-success" value="Sumar"></td>
							</tr>
						<?php  } ?>
					</form>
				</tbody>	
			</table>
		</div>
	<?php  }elseif ($res == 2) { ?>
		<br>
		<h1>Restar producto</h1>
		<br>
		<div class="container">
		<table class="table">
				<thead>
					<tr>
						<td>n</td>
						<td>Nombre</td>
						<td>Precio</td>
						<td>Cantidad</td>
						<td>Descripcion</td>
					</tr>
				</thead>
				<tbody>
					<form action="<?=base_url('producto_controller/agregar')?>" method="POST" onsubmit="return operacion()==true">
						<?php $n=1 ; foreach ($produc as $m) { ?>
							<input type="hidden" name="id" value="<?= $m->id_producto ?>">
							<input type="hidden" name="res" value="<?= $res ?>">
							<tr>
								<td><?= $n;$n++ ?></td>
								<td><?= $m->nombre ?></td>
								<td><?= $m->precio ?></td>
								<td><?= $m->stock ?></td>
								<td><input type="number" name="cant" id="cant"></td>
								<td><input type="submit" class="btn btn-info" value="Restar"></td>
							</tr>
						<?php  } ?>
					</form>
				</tbody>	
			</table>
		</div>
	<?php } ?>
</center>