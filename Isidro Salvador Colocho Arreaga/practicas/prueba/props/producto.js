function categoria() {
	var nombre = document.getElementById('nombre_c').value;
	var des = document.getElementById('descripcion').value;

	if (nombre.length ==0) {
		document.getElementById('nombre_c').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('nombre_c').style.boxShadow='0 0 15px green';
	}
	if (nombre.length >50) {
		document.getElementById('nombre_c').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('nombre_c').style.boxShadow='0 0 15px green';
	}

	if (des.length ==0) {
		document.getElementById('descripcion').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('descripcion').style.boxShadow='0 0 15px green';
	}
	if (des.length >50) {
		document.getElementById('descripcion').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('descripcion').style.boxShadow='0 0 15px green';
	}
	return true;
}

function producto() {
	var nombre = document.getElementById('nombre_p').value;
	var precio = document.getElementById('precio').value;
	var stock = document.getElementById('stock').value;
	var categoria = document.getElementById('categoria').value;

	if (nombre.length ==0) {
		document.getElementById('nombre_p').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('nombre_p').style.boxShadow='0 0 15px green';
	}
	if (nombre.length >50) {
		document.getElementById('nombre_p').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('nombre_p').style.boxShadow='0 0 15px green';
	}

	if (precio.length ==0) {
		document.getElementById('precio').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('precio').style.boxShadow='0 0 15px green';
	}
	if (stock ==0) {
		document.getElementById('stock').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('stock').style.boxShadow='0 0 15px green';
	}
	if (categoria ==0) {
		document.getElementById('categoria').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('categoria').style.boxShadow='0 0 15px green';
	}
	return true;
}

function operacion() {
	var cant = document.getElementById('cant').value;

	if (cant ==0) {
		document.getElementById('cant').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('cant').style.boxShadow='0 0 15px green';
	}
	if (cant<0) {
		document.getElementById('cant').style.boxShadow = "0 0 15px red";
		return false;
	}else{
		document.getElementById('cant').style.boxShadow='0 0 15px green';
	}
	
	return true;
}
