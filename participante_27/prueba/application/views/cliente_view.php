	<center>	
		<br>
		<h1>Tabla de Clientes</h1>
		<br>
		<div class="container">
			<button><a href="" class="btn btn-primary" >Nueva Compra</a></button>
			<button><a href="" class="btn btn-info" data-toggle="modal" data-target="#cliente">Agregar Cliente</a></button>
			<table class="table">
				<thead>
					<tr>
						<td>n</td>
						<td>Nombre Completo</td>
						<td>Direccion</td>
						<td>Telefono</td>
						<td>fecha de compra</td>
						<td>Factura</td>
					</tr>
				</thead>
				<tbody>
					<?php $n=1 ; foreach ($mostrar as $m) { ?>
						<tr>
							<td><?= $n;$n++ ?></td>
							<td><?= $m->nombre." ".$m->apellido ?></td>
							<td><?= $m->direccion ?></td>
							<td><?= $m->telefono ?></td>
							<td><?= $m->fecha ?></td>
							<td><a class="btn btn-success btn-sm" href="<?= base_url('cliente_controller/detalle/'.$m->num_factura) ?>"><?= $m->num_factura ?></a></td>
						</tr>
					<?php  } ?>
				</tbody>	
			</table>
		</div>
	</center>
<div id="cliente" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Agregar un nuevo Cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('cliente_controller/cliente')?>" method="POST" onsubmit="return cliente()==true">
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Nombre</label>
		    <input type="text"  class="form-control" id="nombre" name="nombre">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Apellido</label>
		    <input type="text"  class="form-control" id="apellido" name="apellido">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Direccion</label>
		    <input type="text"  class="form-control" id="direccion" name="direccion">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Fecha de Nacimiento</label>
		    <input type="date"  class="form-control" id="fecha" name="fecha" required>
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlInput1">telefono</label>
		    <input type="number" class="form-control" id="cel" name="cel">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Email</label>
		    <input type="Email" class="form-control" id="email" name="email" required>
		  </div>
		  <div>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
			<input type="submit" id="categoriaB" class="btn btn-primary" name="Guardar" value="Guardar">
		</div>
		</form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>