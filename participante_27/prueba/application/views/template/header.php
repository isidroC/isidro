<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('props/css/bootstrap.min.css')?>">
	<script src="<?= base_url('props/js/jquery-3.3.1.min.js') ?>"></script>
	<script src="<?= base_url('props/js/bootstrap.min.js') ?>"></script>
	<script src="<?= base_url('props/producto.js') ?>"></script>
	<script src="<?= base_url('props/cliente.js') ?>"></script>	
</head>
<body>
	<?php $this->load->view('template/navbar') ?>