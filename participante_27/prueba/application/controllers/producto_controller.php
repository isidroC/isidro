<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class producto_controller extends CI_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model("producto_model");
	}

	public function index(){
		$data = array(
			'title' => 'Tienda || Producto',
			'mostrar' => $this->producto_model->mostrar(),
			'categoria' => $this->producto_model->get_categoria()
		);
		$this->load->view('template/header',$data);
		$this->load->view('producto_view');
		$this->load->view('template/footer');
	}

	public function categoria(){
		$datos['nombre']=$this->input->post('nombre_c');
		$datos['descripcion']=$this->input->post('descripcion');
		$res = $this->producto_model->categoria($datos);
		if ($res=='add') {
			echo '<script>alert("agregado exitosamente")</script>';
		}else{
			echo '<script>alert("Error al agregar")</script>';
		}
		redirect('producto_controller','refresh');
	}

	public function producto(){
		$datos['nombre']=$this->input->post('nombre_p');
		$datos['precio']=$this->input->post('precio');
		$datos['stock']=$this->input->post('stock');
		$datos['categoria']=$this->input->post('categoria');
		$res = $this->producto_model->producto($datos);
		if ($res=='add') {
			echo '<script>alert("agregado exitosamente")</script>';
		}else{
			echo '<script>alert("Error al agregar")</script>';
		}
		redirect('producto_controller','refresh');
	}

	public function operacion($op,$id){
		$data = array(
			'title' => 'Tienda || Producto',
			'mostrar' => $this->producto_model->mostrar(),
			'produc' => $this->producto_model->produc($id),
			'categoria' => $this->producto_model->get_categoria(),
			'res' => $op
		);
		$this->load->view('template/header',$data);
		$this->load->view('operacion_view');
		$this->load->view('template/footer');
	}

	public function agregar(){
		$datos['id']=$this->input->post('id');
		$datos['res']=$this->input->post('res');
		$datos['cant']=$this->input->post('cant');
		$cant=$this->input->post('cant');
		$data = $this->producto_model->obtener($datos["id"]);
		
		$stock = $data->stock;

		if ($stock == $cant) {
			
			echo '<script>alert("se quedara sin producto")</script>';
			
			redirect('producto_controller','refresh');
		
		}elseif ($stock < $cant) {
			
			echo '<script>alert("Es imposible restas mas que el stock")</script>';
			
			redirect('producto_controller','refresh');
		
		}elseif ($stock > $cant) {
			$datos["stock"] = $stock;
			$res = $this->producto_model->agregar($datos);
			if ($res=='add') {
				echo '<script>alert("agregado exitosamente")</script>';
			}else{
				echo '<script>alert("Error al agregar")</script>';
			}
		}
		redirect('producto_controller','refresh');
	}


	public function eliminar($id){
		$res = $this->producto_model->eliminar($id);
		if ($res=='add') {
			echo '<script>alert("eliminado exitosamente")</script>';
		}else{
			echo '<script>alert("Error al eliminar")</script>';
		}
		redirect('producto_controller','refresh');
	}
}
