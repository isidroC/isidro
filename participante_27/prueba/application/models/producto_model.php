<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class producto_model extends CI_Model {

	public function mostrar(){
		$this->db->select('p.id_producto,p.nombre,p.precio,p.stock,c.descripcion');
		$this->db->from('producto p ');
		$this->db->join('categoria c','c.id_categoria=p.id_categoria');
		$exe = $this->db->get();
		return $exe->result();
	}

	public function get_categoria(){
		$exe = $this->db->get('categoria');
		return $exe->result();
	}

	public function categoria($datos){
		$this->db->set('nombre',$datos['nombre']);
		$this->db->set('descripcion',$datos['descripcion']);
		$this->db->insert('categoria');
		if ($this->db->affected_rows() > 0) {
			return 'add';
		}else{
			return false;
		}
	}
	
	public function producto($datos){
		$this->db->set('nombre',$datos['nombre']);
		$this->db->set('precio',$datos['precio']);
		$this->db->set('stock',$datos['stock']);
		$this->db->set('id_categoria',$datos['categoria']);
		$this->db->insert('producto');
		if ($this->db->affected_rows() > 0) {
			return 'add';
		}else{
			return false;
		}
	}

	public function produc($id){
		$this->db->where('id_producto',$id);
		$exe = $this->db->get('producto');
		return $exe->result();
	}

	public function obtener($id){
		$this->db->where('id_producto',$id);
		$exe = $this->db->get('producto');
		return $exe->row();
	}

	public function agregar($datos){
		if ($datos["res"]==1) {
			$this->db->where('id_producto',$datos["id"]);
			$this->db->set('stock',$datos['stock']+$datos["cant"]);
			$this->db->update('producto');
			if ($this->db->affected_rows() > 0) {
				return 'add';
			}else{
				return false;
			}
		}elseif ($datos["res"]==2) {
			$this->db->where('id_producto',$datos["id"]);
			$this->db->set('stock',$datos['stock']-$datos["cant"]);
			$this->db->update('producto');
			if ($this->db->affected_rows() > 0) {
				return 'add';
			}else{
				return false;
			}
		}
	}

	public function eliminar($id){
		$this->db->where('id_producto',$id);
		$this->db->delete('producto');
		if ($this->db->affected_rows() > 0) {
			return 'add';
		}else{
			return false;
		}
	}
}
