
<div>
	<form method="POST" action="<?= base_url('GestionEmpleados/frmEmpleado') ?>">
		<table>
			<tr>
				<td>Id</td>
				<td><input type="text" name="id" id="id"></td>
			</tr>
			<tr>
				<td>Nombres</td>
				<td><input type="text" name="nombres" id="nombres"></td>
			</tr>
			<tr>
				<td>Apellidos</td>
				<td><input type="text" name="apellidos" id="apellidos"></td>
			</tr>
			<tr>
				<td>DUI</td>
				<td><input type="text" name="dui" id="dui"></td>
			</tr>
			<tr>
				<td><input type="submit" name="guardar" value="Guardar"></td>
			</tr>
		</table>
	</form>
</div>
<div><br><br>
	<table border="1px">
		<thead>
			<tr>
				<td>Id</td>
				<td>Nombre completo</td>
				<td>DUI</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($empleado as $e) { ?>
				<tr>
					<td><?= $e->id ?></td>
					<td><?= $e->nombres." ".$e->apellidos ?></td>
					<td><?= $e->dui ?></td>
					<!--Eliminar-->
					<td><a href="<?= base_url('GestionEmpleados/delEmpleado/').$e->id ?>" onclick="return confirm('Desea eliminar este empleado?');"><button>Eliminar</button></a></td>
					<!--Editar-->
					<td><a href="<?= base_url('GestionEmpleados/getEmpleado/').$e->id ?>" ><button>Editar</button></a></td>
				</tr>
				<?php }  ?>
		</tbody>
	</table>
</div>


<?php 
//Alertas
	if (isset($msj)) {
		//Guardar
		if ($msj=='add') {
			header('refresh:1; url=http://localhost/daotuto');
			 ?>
			 <script> alert('Exito al guardar');</script>
			<?php 
		}
		//Editar
		if ($msj=='edi') {
			header('refresh:1; url=http://localhost/daotuto');
			 ?>
			 <script> alert('Exito al editar');</script>
			<?php 
		}
		//eliminar
		if ($msj=='eli') {
			header('refresh:1; url=http://localhost/daotuto');
			 ?>
			 <script> alert('Exito al eliminar');</script>
			<?php 
		}
		//error
		if ($msj=='error') {
			header('refresh:1; url=http://localhost/daotuto');
			 ?>
			 <script> alert('Error al realizar proceso');</script>
			<?php 
		}
	}
 ?>