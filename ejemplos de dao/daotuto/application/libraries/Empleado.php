<?php 


class Empleado
{
	
	function __construct()
	{
	}

	private $id;
	private $nombres;
	private $apellidos;
	private $dui;

	public function getId(){
		return $this->id;
	}
	public function setId($id){
		$this->id= $id;
	}

	public function getNombres(){
		return $this->nombres;
	}
	public function setNombres($nombres){
		$this->nombres= $nombres;
	}

	public function getApellidos(){
		return $this->apellidos;
	}
	public function setApellidos($apellidos){
		$this->apellidos= $apellidos;
	}

	public function getDui(){
		return $this->dui;
	}
	public function setDui($dui){
		$this->dui= $dui;
	}
	

}//Fin clase
