<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GestionEmpleados extends CI_Controller {
 //Funcion constructora incluimos la entidad( clase Empleado) y el DAO(el modelo empleadoDao)
	public function __construct(){
		parent::__construct();
		$this->load->model('empleadoDao');
		$this->load->library('Empleado');
	}

	/*Fn: Mostrar vista principal de la GestionEmpleados
	@param: na
	@return: vista principal*/
	public function index()
	{
		$data = array (
			'title' => 'Empleado',
			'empleado' => $this->empleadoDao->read()
		);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view('empleado_view');
		$this->load->view('templates/footer');
	}

	/*Fn: frmEmpleado / Ingresar empleado
	@param: campos del formulario id,nombres,apellidos,dui
	@return: msj de ingresar/vista principal*/
	public function frmEmpleado(){

		$e = new Empleado;

		$e->setId($this->input->post('id'));
		$e->setNombres($this->input->post('nombres'));
		$e->setApellidos($this->input->post('apellidos'));
		$e->setDui($this->input->post('dui'));

		$respuesta=$this->empleadoDao->create($e);

		$data = array (
			'title' 	=> 'Empleado',
			'empleado'	=> $this->empleadoDao->read(),
			'msj'   	=> $respuesta
		);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view('empleado_view');
		$this->load->view('templates/footer');
	}

	/*Fn: DelEmpleado / elimina empleado
	@param: id del empleado 
	@return: msj de eliminar /vista principal*/
	public function delEmpleado($id){

		$respuesta=$this->empleadoDao->delete($id);

		$data = array (
			'title' 	=> 'Empleado',
			'empleado'	=> $this->empleadoDao->read(),
			'msj'   	=> $respuesta
		);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view('empleado_view');
		$this->load->view('templates/footer');
	}

	/*Fn: getEmpleado / obtiene registro de un empleado
	@param: id del empleado 
	@return: arreglo con registro de empleado */
	public function getEmpleado($id){
 		$re=$this->empleadoDao->readById($id);

		$datos = array (
			'id' 			=> $re->getId(),
			'nombres' 		=> $re->getNombres(),
			'apellidos' 	=> $re->getApellidos(),
			'dui' 			=> $re->getDui()
		);

		$data = array (
			'title' 	=> 'Actualizar Empleado',
			'datos'		=> $datos
		);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view('empleado_viewAct');
		$this->load->view('templates/footer');
	}

	/*Fn: actEmpleado / actualiza registro de un empleado
	@param: campos de formulario en actualizar 
	@return: arreglo con registro de empleado */
	public function actEmpleado(){
 		$e = new Empleado;

 		$oldid=$this->input->post('idold');
		$e->setId($this->input->post('id'));
		$e->setNombres($this->input->post('nombres'));
		$e->setApellidos($this->input->post('apellidos'));
		$e->setDui($this->input->post('dui'));

		$respuesta=$this->empleadoDao->update($e,$oldid);

		$data = array (
			'title' 	=> 'Empleado',
			'empleado'	=> $this->empleadoDao->read(),
			'msj'   	=> $respuesta
		);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view('empleado_view');
		$this->load->view('templates/footer');
	}

}
