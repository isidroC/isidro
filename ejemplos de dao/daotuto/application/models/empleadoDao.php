<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class empleadoDao extends CI_Model {

	/*Fn: incluiremos la entidad(clase Empleado)
	@param: na
	@return: na*/
	public function __construct(){
		parent:: __construct();
		$this->load->library('Empleado');
	}

	/*Fn: create / Ingresar empleado
	@param: objeto de la clase Empleado creado en el controlador
	@return: respuesta de ingresado*/
	public function create($objetoEmpleado){

		$this->db->set('id',$objetoEmpleado->getId());
		$this->db->set('nombres',$objetoEmpleado->getNombres());
		$this->db->set('apellidos',$objetoEmpleado->getApellidos());
		$this->db->set('dui',$objetoEmpleado->getDui());
		$this->db->insert('empleado');

		if ($this->db->affected_rows()>0) {
			return 'add';
		}else{
			return 'error';
		}
	}

	/*Fn: read / mostrar empleado
	@param: na
	@return: arreglo con datos tabla empleado*/
	public function read(){
		return $this->db->get('empleado')->result();
	}

	/*Fn: update / actualiza empleado
	@param: objeto de la clase Empleado creado en el controlador
	@return: respuesta de actualizado*/
	public function update($objetoEmpleado,$idold){

		$this->db->where('id',$idold);
		$this->db->set('id',$objetoEmpleado->getId());
		$this->db->set('nombres',$objetoEmpleado->getNombres());
		$this->db->set('apellidos',$objetoEmpleado->getApellidos());
		$this->db->set('dui',$objetoEmpleado->getDui());
		$this->db->update('empleado');

		if ($this->db->affected_rows()>0) {
			return 'edi';
		}else{
			return 'error';
		}
	}

	/*Fn: delete / eliminar empleado
	@param: id de empleado
	@return: respuesta de eliminado*/
	public function delete($id){

		$this->db->where('id',$id);
		$this->db->delete('empleado');

		if ($this->db->affected_rows()>0) {
			return 'eli';
		}else{
			return 'error';
		}
	}

	/*Fn: readById / obtiene el registro de un empleado empleado
	@param: id de empleado
	@return: objeto con el registro seteado*/
	public function readById($id){
		$ax= $this->db->get_where('empleado',array('id' => $id))->row();
		$x= new Empleado;
		$x->setId($ax->id);
		$x->setNombres($ax->nombres);
		$x->setApellidos($ax->apellidos);
		$x->setDui($ax->dui);

		return $x;
	}
}
