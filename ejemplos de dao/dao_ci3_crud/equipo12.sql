-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-01-2020 a las 10:28:28
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `equipo12`
--
CREATE DATABASE IF NOT EXISTS `equipo12` DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE `equipo12`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_comprobar_usuario` (IN `pa_username` VARCHAR(50), IN `pa_password` VARCHAR(60))  BEGIN
SELECT * FROM `usuario` WHERE (username = pa_username AND password = pa_password);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar` ()  BEGIN
SELECT u.id_usuario, u.username, u.correo, c.nombre_competencia FROM usuario u
INNER JOIN competencia c ON c.id_competencia = u.id_competencia;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultarporid` (IN `pa_id_usuario` INT)  BEGIN
SELECT * FROM `usuario` WHERE id_usuario = pa_id_usuario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_uc` ()  BEGIN
SELECT u.username, c.nombre_competencia FROM usuario u
INNER JOIN competencia c ON C.id_competencia = U.id_competencia;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminar` (IN `pa_id_usuario` INT)  BEGIN
DELETE FROM `usuario` WHERE id_usuario = pa_id_usuario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar` (IN `pa_username` VARCHAR(50), IN `pa_password` VARCHAR(60), IN `pa_correo` VARCHAR(100), IN `pa_id_competencia` INT)  BEGIN
INSERT INTO `usuario`(`username`, `password`, `correo`, `id_competencia`) VALUES (pa_username, pa_password, pa_correo, pa_id_competencia);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_modificar` (IN `pa_id_usuario` INT, IN `pa_username` VARCHAR(50), IN `pa_correo` VARCHAR(100), IN `pa_id_competencia` INT)  BEGIN
UPDATE `usuario` SET 	`username`=pa_username,
                        `correo`=pa_correo,
                        `id_competencia`=pa_id_competencia 
WHERE `id_usuario`=pa_id_usuario;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencia`
--

CREATE TABLE `competencia` (
  `id_competencia` int(11) NOT NULL,
  `nombre_competencia` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(100) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `competencia`
--

INSERT INTO `competencia` (`id_competencia`, `nombre_competencia`, `descripcion`) VALUES
(1, 'Trabajo En Equipo', 'Se desea trabajar en equipo'),
(2, 'Lider', 'Tiene poder de liderazgo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `username` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `password` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `correo` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `id_competencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `username`, `password`, `correo`, `id_competencia`) VALUES
(1, 'PEDRO', '202cb962ac59075b964b07152d234b70', 'pdrmendoza007@gmail.com', 2),
(3, 'david', '202cb962ac59075b964b07152d234b70', 'david@corre.com', 1),
(5, 'rocio', '202cb962ac59075b964b07152d234b70', 'rocio@corre.com', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `competencia`
--
ALTER TABLE `competencia`
  ADD PRIMARY KEY (`id_competencia`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `usuario_competencia` (`id_competencia`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `competencia`
--
ALTER TABLE `competencia`
  MODIFY `id_competencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_competencia` FOREIGN KEY (`id_competencia`) REFERENCES `competencia` (`id_competencia`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
