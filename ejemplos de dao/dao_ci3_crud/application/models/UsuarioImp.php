<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuarioImp extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('User');
		$this->load->library('Competition');
	}

	/*Gestion*/
	/*Fn: Devuelve un listado de usuarios
	@param:na
	@return: Lista de usuarios
	*/
	public function get_usuarios()
	{
		$sp_consultar = 'CALL sp_consultar()';
		$query = $this->db->query($sp_consultar);
		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	}

	/*Fn: Quita un usuario de la bd
	@param: id a quitar
	@return: Mensaje de eliminacion
	*/
	public function eliminar($id)
	{
		$sp_eliminar = 'CALL sp_eliminar(?)';
		$arreglo['id_usuario'] = $id;
		$query = $this->db->query($sp_eliminar,$arreglo);
		if ($query) {
			return true;
		}else{
			return false;
		}
	}

	/*Fn: Devuelve un listado de competencias
	@param:na
	@return: Lista de competencias (o Instancia de usuario)
	*/
	public function get_competencias()
	{
		//Aqui se le agrega  mysqli_next_result ()  antes del llamado a la consulta para que el siguiente resultado de la consulta se inicialice o esté listo desde el mismo conn_id anterior;
		mysqli_next_result( $this->db->conn_id );
		$exe = $this->db->get('competencia');
		$query = $exe->result();	//Para obtener un array
		$x = new User();			//Declaramos una instancia de Usuario para llamar la propiedad $competencia
		foreach( $query as $row )	//Iniciamos el foreach para tener acceso a las columnas
		{
			$ax = new Competition();	//Declaramos una instancia de Competition para guardar los datos obtenidos
			$ax->setIdCompetencia($row->id_competencia);			//Seteamos el id de Competencia
			$ax->setNombreCompetencia($row->nombre_competencia);	//Seteamos el nombre de Competencia
			$x->setCompetencia($ax);	//Guardamos la instancia de Competition en la propiedad $competencia
		}
		return $x;		//Retornamos la instancia de usuario
	}

	/*Fn: Agrega un usuario
	@param: Objeto Usuario
	@return: Mensaje de insercion
	*/
	public function insertar($user)
	{
		$sp_insertar = 'CALL sp_insertar(?,?,?,?)';
		$arreglo['username'] = $user->getUsername();
		$arreglo['password'] = $user->getPassword();
		$arreglo['correo'] = $user->getCorreo();
		$arreglo['id_competencia'] = $user->getIdCompetencia();
		$query = $this->db->query($sp_insertar,$arreglo);
		if ($query) {
			return "add";
		}else{
			return "errorA";
		}
	}

	/*Fn: Obtiene un usuario
	@param:id del usuario
	@return: Instancia de usuario
	*/
	public function get_datos($id)
	{
		$sp_consultarporid = 'CALL sp_consultarporid(?)';
		$arreglo['id_usuario'] = $id;
		$query = $this->db->query($sp_consultarporid, $arreglo);
		if ($query->num_rows() > 0) {
			$ax = $query->row();
			$x = new User();
			$x->setIdUsuario($ax->id_usuario);
			$x->setUsername($ax->username);
			$x->setCorreo($ax->correo);
			$x->setIdCompetencia($ax->id_competencia);
			return $x;
		}else{
			return false;
		}
	}

	/*Fn: Actualiza los datos de un usuario
	@param: Objeto Usuario
	@return: Mensaje de insercion
	*/
	public function actualizar($user)
	{
		$sp_modificar = 'CALL sp_modificar(?,?,?,?)';
		$arreglo['id_usuario'] = $user->getIdUsuario();
		$arreglo['username'] = $user->getUsername();
		$arreglo['correo'] = $user->getCorreo();
		$arreglo['id_competencia'] = $user->getIdCompetencia();
		$query = $this->db->query($sp_modificar,$arreglo);
		if ($query) {
			return "modi";
		}else{
			return "errorM";
		}
	}	
}
