<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UsuarioImp');
		$this->load->library('User');
	}

	/*Fn: Mostrar vista principal de Usuario
	@param:na
	@return: Vista Principal
	*/
	public function index()
	{
		$datos['usuarios'] = $this->UsuarioImp->get_usuarios();
		$result = $this->UsuarioImp->get_competencias();
		$datos['competencia'] = $result->getCompetencia();
		$this->load->view('usuarioV',$datos);
	}

	/*Fn: Quita un usuario de la bd
	@param: identificador de usuario
	@return: Vista Principal
	*/
	public function eliminar($id)
	{
		$msj = $this->UsuarioImp->eliminar($id);

		if ($msj == true) {
			echo "<script>alert('Eliminado exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al eliminar')</script>";
		}

		redirect('Usuario/index','refresh');
	}

	/*Fn: Crear un nuevo Usuario
	@param: na
	@return: Vista Principal
	*/
	public function insertar()
	{
		$user = new User();
		$user->setUsername($this->input->post('usuario'));
		$user->setPassword(md5($this->input->post('password')));
		$user->setCorreo($this->input->post('correo'));
		$user->setIdCompetencia($this->input->post('competencia'));

		$msj = $this->UsuarioImp->insertar($user);

		if ($msj == "add") {
			echo "<script>alert('Agregado exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al agregar usuario!!')</script>";
		}

		redirect('Usuario/index','refresh');
	}

	/*Fn: Obtener el listado de usuarios
	@param:identificador de usuario
	@return: Vista de Actualizacion
	*/
	public function get_datos($id)
	{
		$datos['usuarios'] = $this->UsuarioImp->get_datos($id);
		$result = $this->UsuarioImp->get_competencias();
		$datos['competencia'] = $result->getCompetencia();
		$this->load->view('usuarioVact',$datos);
	}

	/*Fn: Actualiza los datos de un usuario
	@param: na
	@return: Vista Principal
	*/
	public function actualizar()
	{
		$user = new User();
		$user->setIdUsuario($this->input->post('id'));
		$user->setUsername($this->input->post('usuario'));
		$user->setCorreo($this->input->post('correo'));
		$user->setIdCompetencia($this->input->post('competencia'));

		$msj = $this->UsuarioImp->actualizar($user);

		if ($msj == "modi") {
			echo "<script>alert('Modificado exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al modificar usuario!!')</script>";
		}

		redirect('Usuario/index','refresh');
	}
}
