<!DOCTYPE html>
<html>
<head>
	<title>Usuario</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('props/bootstrap/css/bootstrap.min.css')?>">
	<script src="<?php echo base_url('props/bootstrap/js/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('props/bootstrap/js/bootstrap.min.js')?>"></script>
</head>
<body>
	<form method="post" action="<?php echo base_url('Usuario/actualizar')?>">
		<table>
			<tr>
				<td>Id</td>
				<td><input type="text" name="id" id="id" class="form-control" readonly="" value="<?php echo $usuarios->getIdUsuario() ?>"></td>
			</tr>			
			<tr>
				<td>Usuario</td>
				<td><input type="text" name="usuario" id="usuario" class="form-control" value="<?php echo $usuarios->getUsername() ?>"></td>
			</tr>
			<tr>
				<td>Correo</td>
				<td><input type="text" name="correo" id="correo" class="form-control" value="<?php echo $usuarios->getCorreo() ?>"></td>
			</tr>
			<tr>
				<td>Competencia</td>
				<td>
					<select name="competencia" id="competencia" class="form-control">
						<option value="">--Seleccione una Competencia--</option>
						<?php foreach ($competencia as $comp) { ?>
							<option value="<?php echo $comp->getIdCompetencia() ?>"<?php if ($comp->getIdCompetencia() == $usuarios->getIdCompetencia()) { echo "selected";} ?>><?php echo $comp->getNombreCompetencia() ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>	
		</table>
		<input type="submit" name="enviar" class="btn btn-primary">
	</form>
</body>
</html>