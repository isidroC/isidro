<!DOCTYPE html>
<html>
<head>
	<title>Usuario</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('props/bootstrap/css/bootstrap.min.css')?>">
	<script src="<?php echo base_url('props/bootstrap/js/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('props/bootstrap/js/bootstrap.min.js')?>"></script>
</head>
<body>
	<form method="post" action="<?php echo base_url('Usuario/insertar')?>">
		<table>
			<tr>
				<td>Usuario</td>
				<td><input type="text" name="usuario" id="usuario" class="form-control"></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type="text" name="password" id="password" class="form-control"></td>
			</tr>
			<tr>
				<td>Correo</td>
				<td><input type="text" name="correo" id="correo" class="form-control"></td>
			</tr>
			<tr>
				<td>Competencia</td>
				<td>
					<select name="competencia" id="competencia" class="form-control">
						<option value="">--Seleccione una Competencia--</option>
						<?php foreach ($competencia as $comp) { ?>
							<option value="<?php echo $comp->getIdCompetencia() ?>"><?php echo $comp->getNombreCompetencia() ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>	
		</table>
		<input type="submit" name="enviar" class="btn btn-primary">
	</form>
	<br>
	<table border="1">
		<thead>
			<tr>
				<th>Usuario</th>
				<th>Correo</th>
				<th>Competencia</th>
				<th colspan="2">Id</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($usuarios as $user) { ?>
				<tr>
					<td><?php echo $user->username; ?></td>
					<td><?php echo $user->correo; ?></td>
					<td><?php echo $user->nombre_competencia; ?></td>
					<!-- <td><?php echo $user->id_usuario; ?></td> -->
					<td><a onclick="return confirm('Desear borrar este registro?');" class="btn btn-danger" href="<?php echo base_url('Usuario/eliminar/'.$user->id_usuario)?>">Eliminar</a></td>
					<td><a class="btn btn-info" href="<?php echo base_url('Usuario/get_datos/'.$user->id_usuario)?>">Modificar</a></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</body>
</html>