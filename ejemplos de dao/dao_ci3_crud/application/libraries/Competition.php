<?php

class Competition{

	private $id_competencia;
    private $nombre_competencia;
	private $descripcion;

    /*G&S*/
    public function getIdCompetencia()
    {
        return $this->id_competencia;
    }

    public function setIdCompetencia($id_competencia)
    {
        $this->id_competencia = $id_competencia;
    }

    public function getNombreCompetencia()
    {
        return $this->nombre_competencia;
    }

    public function setNombreCompetencia($nombre_competencia)
    {
        $this->nombre_competencia = $nombre_competencia;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
}