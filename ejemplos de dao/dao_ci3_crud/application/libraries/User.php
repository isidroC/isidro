<?php

class User{

	private $id_usuario;
    private $username;
	private $password;
    private $correo;
    private $id_competencia;
    //Array de Instancias
    //Tendra una propiedad llamada $competencia del tipo array, que guardaría un array de objetos Competition pasados a través del método setCompetencia().
    private $competencia=array();

    /*Rel*/
    public function getCompetencia()
    {
        return $this->competencia;
    }

    //También se puede verificar que el parámetro pasado al método es del tipo Competition usando instanceof... De esa manera se evita que el método reciba cualquier cosa.
    public function setCompetencia($competencia)
    {
        if ($competencia instanceof Competition) {
            $this->competencia[]=$competencia;
        }
    }

    /*G&S*/
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    public function setIdUsuario($id_usuario)
    {

        $this->id_usuario = $id_usuario;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }

    public function getIdCompetencia()
    {
        return $this->id_competencia;
    }

    public function setIdCompetencia($id_competencia)
    {
        $this->id_competencia = $id_competencia;
    }
}